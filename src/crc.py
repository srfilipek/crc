"""
CRC implementations in python. Slow, but simple and easy to follow.

@author Stefan Filipek
"""


def crc8(data: bytes, poly: int, rem: int = 0):
    """CRC 8 calculation over the given data.

    Args:
        data: Input message.
        poly: CRC polynomial, without the MSb set.
        rem: Carry-over remainder (if any)

    Returns: CRC remainder (check value).
    """
    for byte in data:
        # Add the data and remainder from the last division operation
        rem ^= byte

        # Perform long division over this byte
        for _ in range(8):
            rem <<= 1

            # Should we subtract the divisor/polynomial?
            if rem & 0x100:
                rem ^= poly | 0x100

    return rem


def crc8_rev(data: bytes, poly: int, rem: int = 0):
    """Reversed CRC 8 calculation over the given data.

    Args:
        data: Input message.
        poly: CRC polynomial, without the MSb set.
        rem: Ending remainder from the forward operation.

    Returns: CRC remainder after the reverse operation.
    """
    for byte in reversed(data):
        for _ in range(8):
            if rem & 0x1:
                rem ^= poly | 0x100
            rem >>= 1
        rem ^= byte
    return rem


def crc8_gen(data: bytes, poly: int, rem: int = 0):
    """Like crc8(), but yield all intermediate values"""
    for byte in data:
        rem ^= byte
        for _ in range(8):
            rem <<= 1
            if rem & 0x100:
                rem ^= poly | 0x100
            yield rem


def crc8_rev_gen(data: bytes, poly: int, rem: int = 0):
    """Like crc8_rev(), but yield all intermediate values"""
    for byte in reversed(data):
        for _ in range(8):
            yield rem
            if rem & 0x1:
                rem ^= poly | 0x100
            rem >>= 1
        rem ^= byte


def crc8_error_finder(num_bytes: int, poly: int, rem: int):
    """Work backwards from the syndrome value to find the error bit position.

    Args:
        num_bytes: Number of bytes in the message.
        poly: CRC8 polynomial.
        rem: Syndrome check value.

    Returns: The bit position of the error, or None if not found.
    """
    for bit in reversed(range(num_bytes * 8)):
        if rem == poly:
            return bit
        if rem & 0x1:
            rem ^= poly | 0x100
        rem >>= 1
    return None


def crc8_error_table(num_bytes: int, poly: int, rem: int = 0):
    """Generate a dictionary of syndrome value to the error bit position.

    Args:
        num_bytes: Number of bytes in the message.
        poly: CRC8 polynomial.
        rem: Carry-over remainder (if any).

    Returns: A mapping of syndrome value to bit error position.
    """
    table = {}
    error = bytearray(num_bytes)

    for bit in range(num_bytes * 8):
        error[bit // 8] = 0x80 >> (bit % 8)
        syndrome = crc8(error, poly, rem)

        assert syndrome not in table
        table[syndrome] = bit

        error[bit // 8] = 0

    return table


def crc16(data: bytes, poly: int, rem: int = 0):
    for byte in data:
        rem ^= byte << 8
        for _ in range(8):
            rem <<= 1
            if rem & 0x10000:
                rem ^= poly | 0x10000
    return rem


def crc16_rev(data: bytes, poly: int, rem: int = 0):
    for byte in reversed(data):
        for _ in range(8):
            if rem & 0x1:
                rem ^= poly | 0x10000
            rem >>= 1
        rem ^= byte << 8
    return rem


def crc16_error_finder(num_bytes: int, poly: int, rem: int):
    """Work backwards from the syndrome value to find the error bit position.

    Args:
        num_bytes: Number of bytes in the message.
        poly: CRC16 polynomial.
        rem: Syndrome check value.

    Returns: The bit position of the error, or None if not found.
    """
    for bit in reversed(range(num_bytes * 8)):
        if rem == poly:
            return bit
        if rem & 0x1:
            rem ^= poly | 0x10000
        rem >>= 1
    return None


def crc16_error_table(num_bytes: int, poly: int, rem: int = 0):
    """Generate a dictionary of syndrome value to the error bit position.

    Args:
        num_bytes: Number of bytes in the message.
        poly: CRC16 polynomial.
        rem: Carry-over remainder (if any).

    Returns: A mapping of syndrome value to bit error position.
    """
    table = {}
    error = bytearray(num_bytes)

    for bit in range(num_bytes * 8):
        error[bit // 8] = 0x80 >> (bit % 8)
        syndrome = crc16(error, poly, rem)

        assert syndrome not in table
        table[syndrome] = bit

        error[bit // 8] = 0

    return table
