import unittest

from crc import crc8, crc16

data = b"\x12\x34\x56\x78\x90\x00\xa5\xff"


class Tests(unittest.TestCase):
    def test_crc_8(self):
        self.assertEqual(0x73, crc8(data, 0x07, 0))

    def test_crc8_cdma2000(self):
        self.assertEqual(0x4C, crc8(data, 0x9B, 0xFF))

    def test_crc16_ccitt_false(self):
        self.assertEqual(0xC35D, crc16(data, 0x1021, 0xFFFF))

    def test_crc16_xmodem(self):
        self.assertEqual(0xF263, crc16(data, 0x1021, 0x0000))

    def test_crc16_cdma200(self):
        self.assertEqual(0x2CBA, crc16(data, 0xC867, 0xFFFF))
