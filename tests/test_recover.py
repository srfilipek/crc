import secrets
import unittest
from functools import partial

import crc

POLY_XMODEM = 0x1021
POLY_CDMA = 0x9B


class TestRecover(unittest.TestCase):
    def _syndrome_check(self, num_bytes, crc_func, correction_func):
        data = bytearray(secrets.token_bytes(num_bytes))

        good_crc = crc_func(data)

        for error_pos in range(num_bytes * 8):
            data[error_pos // 8] ^= 0x80 >> (error_pos % 8)  # Set the error

            bad_crc = crc_func(data)
            syndrome = bad_crc ^ good_crc

            calculated = correction_func(syndrome)
            self.assertEqual(error_pos, calculated)

            data[error_pos // 8] ^= 0x80 >> (error_pos % 8)  # Undo the error

    def test_crc8_reverse(self):
        num_bytes = 8
        crc_func = partial(crc.crc8, poly=POLY_CDMA, rem=0)
        correction_func = partial(crc.crc8_error_finder, num_bytes, POLY_CDMA)

        self._syndrome_check(num_bytes, crc_func, correction_func)

    def test_crc8_table(self):
        num_bytes = 8
        table = crc.crc8_error_table(num_bytes, POLY_CDMA)
        crc_func = partial(crc.crc8, poly=POLY_CDMA, rem=0)
        correction_func = table.get

        self._syndrome_check(num_bytes, crc_func, correction_func)

    def test_crc16_reverse(self):
        num_bytes = 512
        crc_func = partial(crc.crc16, poly=POLY_XMODEM, rem=0)
        correction_func = partial(crc.crc16_error_finder, num_bytes, POLY_XMODEM)

        self._syndrome_check(num_bytes, crc_func, correction_func)

    def test_crc16_table(self):
        num_bytes = 512
        table = crc.crc16_error_table(num_bytes, POLY_XMODEM)
        crc_func = partial(crc.crc16, poly=POLY_XMODEM, rem=0)
        correction_func = table.get

        self._syndrome_check(num_bytes, crc_func, correction_func)
